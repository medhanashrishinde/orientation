Basic overview of how Git works:


- create a "repository" (project) with a git hosting tool (like Bitbucket)
- Copy (or clone) the repository to your local machine

- Add a file to your local repo and "commit" (save) the changes

- "Push" your changes to your master branch

- Make a change to your file with a git hosting tool and commit

- "Pull" the changes to your local machine

- Create a "branch" (version), make a change, commit the change

- Open a "pull request" (propose changes to the master branch)

- "Merge" your branch to the master branch

Git Commands :


    git config
    git init
    git clone
    git add
    git commit
    git diff
    git reset
    git status
    git rm
    git log
    git show
    git tag
    git branch
    git checkout
    git merge
    git remote
    git push
    git pull
    git stash
