- Docker is a program for developers to develop, and run applications with containers.


- A Docker image that can be deployed to any docker enviornment is contains everything needed to run an applications as a container. This includes:

code
runtime
libraries
environment variables
configuration files

- A Docker container is a running Docker image.From one image you can create multiple containers .

Docker Commands :


- docker ps -  to view all the containers that are running on the Docker Host

- docker start -  starts any stopped container(s).

- docker stop - stops any running container(s).

- docker run - creates containers from docker images.

- docker rm - deletes the containers.