# Orientation / On Boarding

This is the first step in your journey with us. This module will make you familiar with our workflow.

#### What you will you learn in Orientation module?
1. Basic Git Work-flow
1. Document like a professional coder
1. Docker Basics 
1. Submitting Merge Requests in Gitlab


> **Innovation is seeing what everybody has seen and thinking what nobody has thought**.  
Famous quote by Dr. Albert Szent-Györgyi, discovered Vitamin C.

Our Key **Innovations** are our **Processes**. We have arranged common course material in a structured way, learnt and optimized by training over 300 students to anchor core concepts and master the subject by doing practical assignments.


## How to complete each Module including orientation ? (The Skilling Process)
Learning a skill  is divided into  3 stages(USP) :-
- Stage 1- Understand (U)
- Stage 2 -Summarize (S)
- Stage 3- Practice (P) 



| Process Stage | Description | 
|---------------|-----------|
| Understand    | Read the course material and try to understand topics covered in the Module.           |
| Summarize (Assignment)     | Reinforce critical parts of the subject by writing them down in concise  Summaries. |
| Practice  (Assignment)    | Practice skills, to complete the learning process. This is where your brain learns the most.     |
| Submit Assignments  | Submitted assignments are graded and get feedback from the mentors. |

### Navigating  This Skilling Course in Gitlab 

1. Understand Stage: Find the Reading Material for every Module in the Module Wiki.
2. Summarize Assignments: Find the Summarize Assignments in the Module Issues.
3. Practice Assignments: Find the Practice Assignments in the Module Issues. 
4. Submitting Assignments: Find the Issue to submit Assignments in the Module Issues.

![Gitlab pointers](/extras/01.png)


## Asking Questions/Queries about the Module
Ask your questions in the respective Module Issues

![Gitlab pointers](/extras/03.png)

------------------------------------------------

# Have Questions about the Process ?
* Checkout our [FAQ section](https://gitlab.com/iotiotdotin/orientation/-/wikis/FAQ)
